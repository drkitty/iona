#!/usr/bin/env python3


import sys

from iona.lexer import EOF, Lexer, MmapReader


if len(sys.argv) != 2:
    raise Exception("Usage: lex.py filename")

with open(sys.argv[1], 'rb') as f:
    with MmapReader(f.fileno()) as r:
        l = Lexer(r)
        for t in l:
            print(repr(t))
