#!/usr/bin/env bash


fail() { echo $!; exit 1; }

for name in $(basename -a tests/*); do
    if [[ -d tests/$name ]]; then continue; fi
    echo $name
    tests/$name || fail "Test $name failed"
done
