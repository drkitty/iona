class Constant(object):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name


class SavepointThing(object):
    def save(self):
        raise Exception("Not implemented")

    def roll_back(self, sp):
        raise Exception("Not implemented")
