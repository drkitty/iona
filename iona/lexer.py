import mmap
import os
from copy import copy

from iona.utils import Constant, SavepointThing


EOF = Constant("EOF")


class MmapReader(SavepointThing):
    """UTF-8 only."""

    def __init__(self, fd):
        self.size = os.stat(fd).st_size
        self.map = mmap.mmap(fd, self.size, access=mmap.ACCESS_READ)
        self.pos = 0

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.map.close()
        return False  # Pass exceptions through.

    def __iter__(self):
        i = copy(self)
        i.pos = 0
        del i.__enter__
        del i.__exit__
        return i

    def __next__(self):
        c = self.read(1)
        if c is EOF:
            raise StopIteration
        return c

    def read(self, n):
        s = []
        for _ in range(n):
            if self.pos >= self.size:
                if s:
                    break
                else:
                    return EOF
            leading = self.map[self.pos]
            if leading < 0xC0:
                cont = 0
            elif leading < 0xE0:
                cont = 1
            elif leading < 0xF0:
                cont = 2
            else:
                cont = 3
            if self.pos + cont >= self.size:
                raise Exception("Invalid UTF-8")
            for i in range(1, cont + 1):
                if self.map[self.pos + i] < 0x80:
                    raise Exception("Invalid UTF-8")
            s.append(self.map[self.pos : self.pos + cont + 1].decode('utf8'))
            self.pos += cont + 1
        return "".join(s)

    def peek(self):
        """This is gross."""
        sp = self.save()
        c = self.read(1)
        self.roll_back(sp)
        return c

    def read_while(self, cond):
        s = []
        while True:
            p = self.pos
            c = self.read(1)
            if c is EOF:
                if s:
                    break
                else:
                    return EOF
            elif not cond(c):
                self.pos = p
                break
            s.append(c)
        return "".join(s)

    def save(self):
        return self.pos

    def roll_back(self, sp):
        self.pos = sp


def in_char_range(c, first, last):
    return ord(first) <= ord(c) <= ord(last)


def is_sym_head_char(c):
    return (
        in_char_range(c, "A", "Z") or in_char_range(c, "a", "z")
        or ord(c) >= 0x80
    )


def is_sym_mid_char(c):
    return is_sym_last_char(c) or c == "-"


def is_sym_last_char(c):
    return is_sym_head_char(c) or in_char_range(c, "0", "9")


class String(str):
    pass


class Lexer(SavepointThing):
    """End of line is LF."""

    def __init__(self, reader):
        self.reader = reader

    def __iter__(self):
        i = copy(self)
        i.reader = copy(i.reader)
        return i

    def __next__(self):
        t = self.read()
        if t is EOF:
            raise StopIteration
        return t

    def read(self):
        r = self.reader.save()
        h = self.reader.read(1)
        if h is EOF:
            ### EOF ###
            return EOF
        elif h == " ":
            tail = self.reader.read_while(lambda c: c == " ")
            if tail is EOF:
                tail = ""
            return " " + tail
        elif h in "+-*/()[]\n":
            ### miscellaneous ###
            return h
        elif h == ":":
            ### single or double colon ###
            sp = self.reader.save()
            t = self.reader.read(1)
            if t == ":":
                return "::"
            else:
                self.reader.roll_back(sp)
                return ":"
        elif is_sym_head_char(h):
            ### symbol ###
            tail = self.reader.read_while(is_sym_mid_char)
            if tail and (tail is not EOF) and (not is_sym_last_char(tail[-1])):
                raise Exception("Invalid symbol \"{}\"".format(h + tail))
            return h + tail
        elif in_char_range(h, "1", "9"):
            ### decimal integer ###
            tail = self.reader.read_while(lambda c: in_char_range(c, "0", "9"))
            return int(h + tail)
        elif h == "0":
            ### non-decimal integer or zero ###
            sp = self.reader.save()
            c = self.reader.read(1)
            base = None
            if c == "x":
                cond = lambda c: (
                    in_char_range(c, "0", "9")
                    or in_char_range(c, "A", "F")
                    or in_char_range(c, "a", "f")
                    or c == "_"
                )
                base = 16
            elif c == "c":
                cond = lambda c: in_char_range(c, "0", "7") or c == "_"
                base = 8
            elif c == "n":
                cond = lambda c: c in "01_"
                base = 2
            else:
                self.reader.roll_back(sp)
                return 0
            tail = self.reader.read_while(cond)
            if tail is EOF:
                raise Exception("Invalid integer literal")
            return int(tail.strip("_"), base)
        elif h == "\"":
            ### string ###
            s = ""
            while True:
                c = self.reader.read(1)
                if c == "\"":
                    break
                elif c == "\\":
                    c = self.reader.read(1)
                    if c == "n":
                        c = "\n"
                if c is EOF:
                    raise Exception("Incomplete string literal")
                s += c
            return String("".join(s))
        else:
            ### invalid ###
            raise Exception("Invalid token")
