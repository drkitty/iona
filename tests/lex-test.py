#!/usr/bin/env python3


import sys
sys.path[0] = ""

from itertools import count
from sys import stderr, stdout

from iona.lexer import EOF, Lexer, MmapReader, String


def compare_lists(x, y):
    xl = len(x)
    yl = len(y)
    rr = []
    for i in count():
        if i < xl:
            xr = repr(x[i])
        else:
            xr = "--"
        if i < yl:
            yr = repr(y[i])
        else:
            yr = "--"
        if xr == yr == "--":
            break
        else:
            rr.append((xr, yr))
    maxlen = 0
    for xr, yr in rr:
        maxlen = max(maxlen, len(xr), len(yr))
    stdout.flush()
    for xr, yr in rr:
        stderr.write("{{:>{0}}}  {{:<{0}}}\n".format(maxlen).format(xr, yr))
    raise Exception("Lists are not equal")


tests = [
    ("1.ioa", [
        "a", " ", "::", " ", "int", " ", "box", "\n",
        "a", " ", ":", " ", 99, "\n",
    ]),
    ("2.ioa", [
        "foo", "(", "bar", "(", 1, " ", "/", " ", "zom", "(",
        3, " ", "-", " ", 4, ")", ")", "  ", "*", "  ", 300, ")", "\n",
    ]),
    ("3.ioa", [
        "name", " ", "::", " ", String("Simon"), "\n",
        "x", " ", "::", " ", "name", " ", "+", " ",
        String(" says:\nAlways write tests!\n"), "\n",
    ]),
]

for src, tokens_ref in tests:
    with open("tests/lex-tests/" + src, "rb") as f:
        with MmapReader(f.fileno()) as r:
            l = Lexer(r)

            tokens = []
            while True:
                t = l.read()
                if t is EOF:
                    break
                tokens.append(t)
            if tokens != tokens_ref:
                compare_lists(tokens, tokens_ref)
